#ifndef QuantumEvalOp_h
#define QuantumEvalOp_h

// for Windows development only
#define SIMULATION

class QuantumEvalOp : public EvaluateOp
{
public:
	void registerParameters(StateP);
	FitnessP evaluate(IndividualP individual);
	bool initialize(StateP);

	StateP state_;

	uint nWires;
	uint nWiresA, nWiresB;
	uint nGatesA, nGatesB;
};
typedef boost::shared_ptr<QuantumEvalOp> QuantumEvalOpP;


#endif // QuantumEvalOp_h
