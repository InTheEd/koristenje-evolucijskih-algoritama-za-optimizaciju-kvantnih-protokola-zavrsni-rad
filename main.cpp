#include <ecf/ECF.h>
#include "QuantumEvalOp.h"
#include "IntGenotype.h"
#include "WriteBest.h"


int main(int argc, char** argv)
{
	StateP state (new State);

	// set the evaluation operator
	state->setEvalOp(new QuantumEvalOp);

	IntGenotypeP gen(new IntGenotype::IntGenotype);
	state->addGenotype(gen);

	WriteBest* op = new WriteBest;
	state->addOperator((OperatorP) op);

	state->initialize(argc, argv);

	state->run();

	return 0;
}
