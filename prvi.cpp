#include <ecf/ECF.h>
#include "OneMaxEvalOp.h"
#include "Quantum/Quantum.h"

int main(int argc, char **argv)
{

	int NUM_WIRES = 4;
	quantum::DensityList dl;
	quantum::KetBra kb(NUM_WIRES); // 4 wires

	for(int i=0; i<NUM_WIRES; ++i)
	dl.dimension.push_back(2);  // wires are dimenion 2; total dimension = 2^4

	kb.p = 1;
	dl.density.push_back(kb);   // system initialized to all zero state with probability "p = 1"

	std::cout << "BEFORE:\n";
	dl.print(std::cout);

	dl.applyHadamard(1);  // apply H gate to wire "1" (second wire; 0 indexing)

	std::cout << "AFTER:\n";
	dl.print(std::cout);


	double entropy1 = dl.entropy();
	dl.measure(1);
	double entropy2 = dl.entropy();

	std::cout << "Entropy before measure (should be 0): " << entropy1 << "\n";
	std::cout << "Entropy before measure (should be 1): " << entropy2 << "\n\n";


	StateP state (new State);

	// set the evaluation operator
	state->setEvalOp(new OneMaxEvalOp);

	state->initialize(argc, argv);
	state->run();

	return 0;
}

