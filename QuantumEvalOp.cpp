#include <ecf/ECF.h>
#include "QuantumEvalOp.h"
#include "IntGenotype.h"
#include <list>
#include <sstream>


// global constants (extracted from computeFitness)
const int keyA = 0;
const int arA = 1;
const int extraA = 2;
const int numExtraA = 0;

const int T = extraA+numExtraA;

const int keyB = T+1;
const int arB = T+2;
const int extraB = T+3;
const int numExtraB = 0;

const int E = extraB+numExtraB;

const int NUM_WIRES = E+1;

const int ASpace = T;  // largest wire (inclusive) A is allowed access to


// algorithm variants related to fitness
bool CONDITIONING = true;
bool COMPUTE_ACTUAL_RATE = false;


// parts not included in Win environment
#ifndef WIN32

#include "Quantum/Quantum.h"
#include "attack.h"

QKD::Attack* atk;
std::vector <algebra::mat> EVec;

void setupAttackVectors(QKD::Stats& stats) // run this once at startup!
{
  EVec.resize(4);
  for(int i=0; i<EVec.size(); ++i)
    EVec[i].create(4,1);

  atk = new QKD::Attack(stats);
}

#endif


namespace QKD
{
	const int nGateTypes = 4;

  enum GateType
  {
    NOT=0,
    Rotation,
    Hadamard,

    Measure
  };

  struct Gate
  {
    Gate()
    {
      type = 0;
      controlMode = false;
      target = 0;
      control = 0;
      p = 0;
      theta = 0;
      psi = 0;
    }
    int type;
    bool controlMode;
    int target, control;
    double p, theta, psi;
  };
}

double SafeLog(double x)
{
  if(x < 0.000000001)
    return 0;
  return log(x) / log(2.0);
}
double entropy(double x)
{
  return -x*SafeLog(x) - (1-x)*SafeLog(1-x);
}
double entropy(double a, double b, double c, double d)
{
  return -a*SafeLog(a) - b*SafeLog(b) - c*SafeLog(c) - d*SafeLog(d);
}

#ifdef WIN32

double ComputeFitness(std::list <QKD::Gate>& A, std::list <QKD::Gate>& B)
{
	return 1;
}

#else

double ComputeFitness(std::list <QKD::Gate>& A, std::list <QKD::Gate>& B)
{
/*
  const int keyA = 0;
  const int arA = 1;
  const int extraA = 2;
  const int numExtraA = 0;

  const int T = extraA+numExtraA;

  const int keyB = T+1;
  const int arB = T+2;
  const int extraB = T+3;
  const int numExtraB = 0;

  const int E = extraB+numExtraB;

  const int NUM_WIRES = E+1;

  const int ASpace = T;  // largest wire (inclusive) A is allowed access to
*/
  quantum::DensityList dl;

  for(int i=0; i<NUM_WIRES; ++i){
    if(i != E)
      dl.dimension.push_back(2);
    else
      dl.dimension.push_back(4);
  }

  quantum::KetBra kb(NUM_WIRES);
  kb.p = 1;
  dl.density.push_back(kb);

  // run A:
  if(!A.empty()){
    std::list<QKD::Gate>::iterator Iter;
    for(Iter = A.begin(); Iter != A.end(); ++Iter){
      int target = Iter->target;
      int control = Iter->control;
      if(!Iter->controlMode)
	control = -1;

      // ensure that gates aren't applied to wires that cannot be accessed:
      if(target > ASpace || control > ASpace)
	continue;
      if(target == E || control == E)
	continue;

      if(Iter->controlMode && control == target)
	continue;

      switch(Iter->type){
      case QKD::Hadamard: dl.applyHadamard(target, control);
	break;

      case QKD::NOT: dl.applyNOT(target, control);
	break;

      case QKD::Rotation: dl.applySU2(target, control, Iter->p, Iter->theta, Iter->psi);
	break;

      case QKD::Measure: dl.measure(target);
	break;
      }
    }
  }

  dl.applyAttackOp(T, E);  // E attacks

  // Run B:
  if(!B.empty()){
    std::list<QKD::Gate>::iterator Iter;
    for(Iter = B.begin(); Iter != B.end(); ++Iter){
      int target = Iter->target;
      int control = Iter->control;
      if(!Iter->controlMode)
	control = -1;

      // ensure that gates aren't applied to wires that cannot be accessed:
      if(target < T || (control < T && control >= 0))
	continue;
      if(target == E || control == E)
	continue;

      if(Iter->controlMode && control == target)
	continue;

      switch(Iter->type){
      case QKD::Hadamard: dl.applyHadamard(target, control);
	break;

      case QKD::NOT: dl.applyNOT(target, control);
	break;

      case QKD::Rotation: dl.applySU2(target, control, Iter->p, Iter->theta, Iter->psi);
	break;

      case QKD::Measure: dl.measure(target);
	break;
      }
    }
  }

  // now A measures key
  dl.measure(keyA);

  // B measures key
  dl.measure(keyB);

  // project to both users accepting (conditioning):
  if(CONDITIONING == true) {
  dl.project(arA, 1);
  dl.project(arB, 1);
  }

  // check that there is something here (always possible to "reject" with probability 1)
  if(dl.density.empty())
    return 0;  // both parties are always saying 'reject' so no key so key-rate = 0.

  atk->begin();

  quantum::DensityList saveDL = dl;
  double bestRate = 2;

  while(!atk->getNext(EVec[0], EVec[1], EVec[2], EVec[3])){
    dl = saveDL;
    double paccept = dl.trace(EVec, E);
    if(paccept <= 0.00000001)
      return 0;
    // compute H(A|B):
    
    double p00 = dl.calculatePr(keyA, 0, keyB, 0, EVec, E);
    double p01 = dl.calculatePr(keyA, 0, keyB, 1, EVec, E);
    double p10 = dl.calculatePr(keyA, 1, keyB, 0, EVec, E);
    double p11 = dl.calculatePr(keyA, 1, keyB, 1, EVec, E);
    
    double HAB = entropy(p00, p01, p10, p11);
    double HB = entropy(p00 + p10);

    // compute S(A|E):
    // trace out B's wires:
    for(int w=keyA+1; w<E; ++w)
      dl.trace(w);

    // trace out A's extra wires:
    //for(int w=keyA+1; w<=T; ++w)
    //  dl.trace(w);

    double SAE = dl.entropy(EVec, E);
    dl.trace(keyA);
    double SE = dl.entropy(EVec, E);
    double rate = 100;

    if(COMPUTE_ACTUAL_RATE)
      rate = (SAE - SE) - (HAB - HB);
    else
      rate = paccept * ( (SAE-SE) - (HAB-HB) );

    if(rate < bestRate)
      bestRate = rate;
  }

  return bestRate;
}

#endif

void testSimple()
{
  std::list <QKD::Gate> A, B;
  QKD::Gate g;
  g.type = QKD::NOT;
  g.target = 2;
  A.push_back(g);
  ComputeFitness(A,B);
}

void testBB84()
{
  std::list <QKD::Gate> A, B;
  QKD::Gate g;
  g.type = QKD::NOT;
  g.target = 1;
  g.controlMode = false;

  A.push_back(g); // g1

  g.type = QKD::Hadamard;
  g.target = 0;
  g.controlMode = false;

  A.push_back(g); // g2

  g.type = QKD::NOT;
  g.target = 2;
  g.controlMode = true;
  g.control = 0;

  A.push_back(g); // g3


  g.type = QKD::NOT;
  g.target = 3;
  g.control = 2;
  g.controlMode = true;

  B.push_back(g); // h1

  g.type = QKD::NOT;
  g.target = 4;
  g.controlMode = false;

  B.push_back(g); // h2


  std::cout << "Test 1: key-rate (should be 1) = " << ComputeFitness(A,B) << "\n\n";
}

void test()
{
  std::list <QKD::Gate> A, B;
  QKD::Gate g;
  g.type = 2;
  g.target = 0;
  g.control = 1;
  g.controlMode = false;
  A.push_back(g); // g1

  g.type = 2;
  g.target = 0;
  g.controlMode = true;
  g.control = 0;
  A.push_back(g); // g2

  g.type = 1;
  g.target = 2;
  g.controlMode = false;
  g.control = 1;
  A.push_back(g); // g3


  g.type = 1;
  g.target = 4;
  g.control = 4;
  g.controlMode = true;
  B.push_back(g); // h1

  g.type = 1;
  g.target = 4;
  g.controlMode = false;
  g.control = 3;
  B.push_back(g); // h2


  std::cout << "Test 1: key-rate = " << ComputeFitness(A,B) << "\n\n";
}


//
// evaluation code
//

void QuantumEvalOp::registerParameters(StateP state)
{
	state->getRegistry()->registerEntry("noise", (voidP) (new double(0.1)), ECF::DOUBLE, "symmetric channel noise level");
	state->getRegistry()->registerEntry("CONDITIONING", (voidP) (new uint(1)), ECF::UINT, "conditioning");
	state->getRegistry()->registerEntry("COMPUTE_ACTUAL_RATE", (voidP) (new uint(0)), ECF::UINT, "COMPUTE_ACTUAL_RATE");
	state->getRegistry()->registerEntry("CHANNEL", (voidP) (new std::string("symmetric")), ECF::STRING, "symmetric testChannel1 testChannel2");
	state->getRegistry()->registerEntry("numOfGates", (voidP)(new uint(0)), ECF::UINT, "number of gates permitted on circuit");
}


bool QuantumEvalOp::initialize(StateP state)
{
	nWires = NUM_WIRES;

	nWiresA = T + 1;
	nWiresB = E - T;

	nGatesA = 3;
	nGatesB = 2;

	state_ = state;

	voidP sptr = state->getRegistry()->getEntry("noise");
	double noise = *((double*)sptr.get());

	sptr = state->getRegistry()->getEntry("CONDITIONING");
	CONDITIONING = *((uint*)sptr.get()) ? true : false;

	sptr = state->getRegistry()->getEntry("COMPUTE_ACTUAL_RATE");
	COMPUTE_ACTUAL_RATE = *((uint*)sptr.get()) ? true : false;

	sptr = state->getRegistry()->getEntry("CHANNEL");
	std::string channel = *((std::string*)sptr.get());

#ifndef WIN32
	QKD::Stats stats;

	if(channel == "testChannel1")
		stats.testChannel1();
	else if(channel == "testChannel2")
		stats.testChannel2();
	else
		stats.symmetric(noise);  // Test EA with different values between .01 and .5.

	setupAttackVectors(stats);

	testBB84();

	std::cout << "Above value should be = " << stats.BB84Rate() << "\n";
#endif

	return true;
}


extern bool evaluateVerbose;

FitnessP QuantumEvalOp::evaluate(IndividualP individual)
{
    FitnessP fitness (new FitnessMax);

	IntGenotype::IntGenotype* genType = (IntGenotype::IntGenotype*) individual->getGenotype(0).get();
	IntGenotype::IntGenotype* genTarget = (IntGenotype::IntGenotype*) individual->getGenotype(1).get();
	BitString::BitString* bitstr = (BitString::BitString*) individual->getGenotype(2).get();
	IntGenotype::IntGenotype* genControl = (IntGenotype::IntGenotype*) individual->getGenotype(3).get();
	FloatingPoint::FloatingPoint* flpoint = (FloatingPoint::FloatingPoint*) individual->getGenotype(4).get();

	std::list <QKD::Gate> A, B;
	QKD::Gate gate;

	int size = genType->nSize_;
    	nGatesA = (int)(size / 2);
	nGatesB = size - nGatesA;

	for(uint i = 0; i < nGatesA; i++) {
		// decode gate type, target, control mode
		gate.type = genType->intValues[i] % QKD::nGateTypes;
		gate.target = genTarget->intValues[i] % nWiresA;
		gate.controlMode = bitstr->bits[i];
		gate.control = genControl->intValues[i] % nWiresA;

		if(gate.type == QKD::Rotation) {
			gate.p = flpoint->realValue[i * 3 + 0];
			gate.theta = flpoint->realValue[i * 3 + 1] * 2 * 3.1415926535;
			gate.psi = flpoint->realValue[i * 3 + 2] * 2 * 3.1415926535;
		}

		A.push_back(gate);
	}

	for(uint i = nGatesA; i < (nGatesA + nGatesB); i++) {
		// decode gate type, target, control mode
		gate.type = genType->intValues[i] % QKD::nGateTypes;
		gate.target = T + genTarget->intValues[i] % nWiresB;
		gate.controlMode = bitstr->bits[i];
		gate.control = T + genControl->intValues[i] % nWiresB;

		if(gate.type == QKD::Rotation) {
			gate.p = flpoint->realValue[i * 3 + 0];
			gate.theta = flpoint->realValue[i * 3 + 1];
			gate.psi = flpoint->realValue[i * 3 + 2];
		}

		B.push_back(gate);
	}

	fitness->setValue(ComputeFitness(A, B));

	if(evaluateVerbose) {
		stringstream ss;
		ss << "Gates A:" << endl;
		std::list<QKD::Gate>::iterator Iter;
		for(Iter = A.begin(); Iter != A.end(); ++Iter) {
			ss << "type " << Iter->type << " target " << Iter->target << " mode " << Iter->controlMode << " control: " << Iter->control;
			if(Iter->type == QKD::Rotation)
				ss << " p: " << Iter->p << " theta: " << Iter->theta << " psi: " << Iter->psi;
			ss << endl;
		}
		ss << "Gates B:" << endl;
		for(Iter = B.begin(); Iter != B.end(); ++Iter) {
			ss << "type " << Iter->type << " target " << Iter->target << " mode " << Iter->controlMode << " control: " << Iter->control;
			if(Iter->type == QKD::Rotation)
				ss << " p: " << Iter->p << " theta: " << Iter->theta << " psi: " << Iter->psi;
			ss << endl;
		}
		ECF_LOG(state_, 1, ss.str());
	}

    // return the smart pointer to new fitness
    return fitness;
}
