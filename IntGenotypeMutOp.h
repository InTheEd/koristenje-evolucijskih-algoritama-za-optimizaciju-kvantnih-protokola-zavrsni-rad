#include <ecf/ECF.h>
#include <ecf/ECF_base.h>
#include <cmath>
#include <sstream>


namespace IntGenotype
{
	class IntGenotypeMutOp : public MutationOp
	{
	public:
		bool mutate(GenotypeP gene);
	};
	typedef boost::shared_ptr<IntGenotypeMutOp> IntGenotypeMutOpP;

}