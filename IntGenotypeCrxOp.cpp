#include <ecf/ECF.h>
#include <ecf/ECF_base.h>
#include <cmath>
#include <sstream>
#include "IntGenotype.h"

namespace IntGenotype
{
	
		bool IntGenotypeCrxOp::mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child) {

			IntGenotype* p1 = (IntGenotype*)(gen1.get());
			IntGenotype* p2 = (IntGenotype*)(gen2.get());
			IntGenotype* ch = (IntGenotype*)(child.get());

			//uint prva = (int) p1->getUBound();
			//uint druga = (int) p2->getUBound();
			//
			//for (uint i = 0; i < p1->intValues.size(); i++) {
			//	if (p1->intValues[i] < prva)
			//		prva = p1->intValues[i];
			//	if (p2->intValues[i] < druga)
			//		druga = p2->intValues[i];
			//}

			//for (uint i = 0; i < p1->intValues.size(); i++) {
			//	p1->intValues[i] = p1->intValues[i] - prva;
			//	p2->intValues[i] = p2->intValues[i] - druga;
			//}

			uint dimensionCrs = state_->getRandomizer()->getRandomInteger((int)p1->intValues.size());
			switch (state_->getRandomizer()->getRandomInteger(0, 1)) {
			case 0: for (uint i = 0; i < dimensionCrs; i++) {
						ch->intValues[i] = p1->intValues[i];
			}
					for (uint i = dimensionCrs; i < p2->intValues.size(); i++) {
						ch->intValues[i] = p2->intValues[i];
					}
					break;
			case 1: for (uint i = 0; i < dimensionCrs; i++) {
						ch->intValues[i] = p2->intValues[i];
			}
					for (uint i = dimensionCrs; i < p1->intValues.size(); i++) {
						ch->intValues[i] = p1->intValues[i];
					}
			}

			return true;
		}
	
}