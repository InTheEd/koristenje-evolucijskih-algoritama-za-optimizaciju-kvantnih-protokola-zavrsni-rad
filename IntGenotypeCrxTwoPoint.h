#include <ecf/ECF.h>
#include <ecf/ECF_base.h>
#include <cmath>
#include <sstream>

namespace IntGenotype
{
	class IntGenotypeCrxTwoPoint : public CrossoverOp
	{
	public:
		bool mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child);
		
	};
	typedef boost::shared_ptr<IntGenotypeCrxTwoPoint> IntGenotypeCrxTwoPointP;

}