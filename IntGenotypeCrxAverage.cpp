#include <ecf/ECF.h>
#include <ecf/ECF_base.h>
#include <cmath>
#include <sstream>
#include "IntGenotype.h"

namespace IntGenotype
{
	
		bool IntGenotypeCrxAverage::mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child) {

			IntGenotype* p1 = (IntGenotype*)(gen1.get());
			IntGenotype* p2 = (IntGenotype*)(gen2.get());
			IntGenotype* ch = (IntGenotype*)(child.get());

			//uint prva = (int) p1->getUBound();
			//uint druga = (int) p2->getUBound();
			//
			//for (uint i = 0; i < p1->intValues.size(); i++) {
			//	if (p1->intValues[i] < prva)
			//		prva = p1->intValues[i];
			//	if (p2->intValues[i] < druga)
			//		druga = p2->intValues[i];
			//}

			//for (uint i = 0; i < p1->intValues.size(); i++) {
			//	p1->intValues[i] = p1->intValues[i] - prva;
			//	p2->intValues[i] = p2->intValues[i] - druga;
			//}

			for (uint i = 0; i < p1->intValues.size(); i++) {
				ch->intValues[i] = (int) (0.5 * (p1->intValues[i]) + 0.5 * (p2->intValues[i]));
			}

			
			return true;
		}
	
}