#include "ecf/ECF_base.h"
#include "AlgRouletteWheel.h"
#include "ecf/ECF_macro.h"


RouletteWheel :: RouletteWheel()
{
	name_ = "RouletteWheel";

	selFitPropOp = static_cast<SelFitnessProportionalOpP> (new SelFitnessProportionalOp);
	selRandomOp = static_cast<SelRandomOpP> (new SelRandomOp);
	selBestOp = static_cast<SelBestOpP> (new SelBestOp);
}


void RouletteWheel :: registerParameters(StateP state)
{
	registerParameter(state, "crxprob", (voidP) new double(0.5), ECF::DOUBLE, "crossover rate");
	registerParameter(state, "selpressure", (voidP) new double(10), ECF::DOUBLE, 
		"selection pressure: how much is the best individual 'better' than the worst");
}


bool RouletteWheel :: initialize(StateP state)
{
	selFitPropOp->initialize(state);
	selRandomOp->initialize(state);
	selBestOp->initialize(state);

	voidP crRateP = getParameterValue(state, "crxprob");
	crxRate_ = *((double*) crRateP.get());

	voidP selPressP = getParameterValue(state, "selpressure");
	selPressure_ = *((double*) selPressP.get());

	selFitPropOp->setSelPressure(selPressure_);

	return true;
}


bool RouletteWheel :: advanceGeneration(StateP state, DemeP deme)
{
	// elitism: copy current best individual
	IndividualP best = selBestOp->select(*deme);
	best = copy(best);

	// select individuals
	std::vector<IndividualP> wheel;
	wheel = selFitPropOp->selectMany(*deme, (uint) deme->size());

	// copy selected to new population
	for(uint i = 0; i < wheel.size(); ++i) 
		wheel[i] = copy(wheel[i]);

	// replace old population
	for(uint i = 0; i < deme->size(); i++)
		replaceWith((*deme)[i], wheel[i]);

	ECF_LOG(state, 5, "Selected individuals:");
	for(uint i = 0; i < deme->size(); i++){
		ECF_LOG(state, 5, dbl2str(deme->at(i)->fitness->getValue()));
	}

	// determine the number of crx operations
	uint noCrx = (int)(deme->size() * crxRate_ /2);

	// perform crossover
	for(uint i = 0; i < noCrx; i++){

		// select parents

		DemeP dem = state->getPopulation()->at(0);
		std::vector<std::vector<IndividualP>> parents;
		std::vector<uint> covered;
		for (uint i = 0; i < dem->size(); i++) {
			IndividualP temp1 = dem->at(i);
			uint size1 = temp1->getGenotype()->getGenomeSize();
			if (std::find(covered.begin(), covered.end(), size1) != covered.end()) {
				continue;
			}
			covered.push_back(size1);
			std::vector<IndividualP> temp_par;
			temp_par.push_back(temp1);
			for (uint j = i+1; j < dem->size(); j++) {
				IndividualP temp2 = dem->at(j);
				uint size2 = temp2->getGenotype()->getGenomeSize();
				if (size1 == size2) {
					temp_par.push_back(temp2);
				}
			}
			if (temp_par.size() > 1) {
				parents.push_back(temp_par);
			}
		}
		int randomIndex = rand() % parents.size();
		int randomIndex2 = rand() % parents[randomIndex].size();
		IndividualP parent1 = parents[randomIndex][randomIndex2];
		int randomIndex3 = rand() % parents[randomIndex].size();
		while (randomIndex3 == randomIndex2) {
			randomIndex3 = rand() % parents[randomIndex].size();
		}
		IndividualP parent2 = parents[randomIndex][randomIndex3];

		ECF_LOG(state, 5, "Parents: " + dbl2str(parent1->fitness->getValue()) + ", " + dbl2str(parent2->fitness->getValue()));

		// create children
		IndividualP child1 = copy(parent1);
		IndividualP child2 = copy(parent2);

		// perform crx operations
		mate(parent1, parent2, child1);
		mate(parent1, parent2, child2);

		// replace parents with children
		replaceWith(parent1, child1);
		replaceWith(parent2, child2);
	}

	// perform mutation on whole population
	mutate(*deme);

	// evaluate new individuals
	for(uint i = 0; i < deme->size(); i++)
		if(!deme->at(i)->fitness->isValid()) {
			evaluate(deme->at(i));
		}

	// elitism: preserve best individual
	IndividualP random = selRandomOp->select(*deme);
	if(best->fitness->isBetterThan(random->fitness))
		replaceWith(random, best);

	return true;
}
