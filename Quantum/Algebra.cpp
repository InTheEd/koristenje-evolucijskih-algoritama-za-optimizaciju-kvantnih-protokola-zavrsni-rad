/*
Algebra.cpp
Written by Walter O. Krawec

Copyright (c) 2016 Walter O. Krawec

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Algebra.h"

extern "C"
{
#include <cblas.h>
#include <lapacke.h>
}


namespace algebra
{
void mat::random_unitary(const std::vector <double>& phi_v, const std::vector <double>& psi_v, const std::vector <double>& chi_v, double alpha)
{
	int dim = chi_v.size();//getRows();
	if(dim != getRows())
		create(dim);
	algebra::mat E, temp, temp2;
	temp.create(dim);
	int index = 0;
	Complex z;
	ident();
	for(int j=1; j<=dim-1; ++j)
	{
		E.create(dim);
		E.ident();
		for(int i=j; i>=1; --i)
		{
			int I2 = i;
			int J = j+1;
			double psi, phi, chi;
			psi = psi_v[index];
			phi = phi_v[index];
			//std::cout << i << ", " << j << " = " << index << "\n";
			if(I2 == 1)
				chi = chi_v[i-1];
			else
				chi = 0;

			//psi = 0;
			//chi = 0;
			//phi = 0;
			//if((i%2)==0) phi = 0;

			++index;
			temp.ident();
			z = algebra::ComplexHelper::construct(psi);
			z *= cos(phi);
			temp(I2-1, I2-1) = z;

			z = algebra::ComplexHelper::construct(chi);
			z *= sin(phi);
			temp(I2-1, J-1) = z;

			z = algebra::ComplexHelper::construct(-chi);
			z *= -sin(phi);
			temp(J-1, I2-1) = z;

			z = algebra::ComplexHelper::construct(-psi);
			z *= cos(phi);
			temp(J-1, J-1) = z;

			//if(j == dim-1 || (I-1 < dim/2 && J-1 == I-1+dim/2)){
			E.multiply(temp, temp2);
			E = temp2;
			//}
		}
		//if(j==dim/3||j==dim/3*2||j == dim-1){
		//if(j==dim-1){
		multiply(E, temp2);
		*this = temp2;
		//}
	}

	Complex c = cos(alpha) + I*sin(alpha);
	multiplyScalar(c);
}

bool matrix::eigenValues(std::vector <double>& values)
{
	int n, lda, info, lwork, lrwork, liwork;
	n = getRows();
	lda = n;
	int iwkopt;
	int* iwork;
	double rwkopt;
	double* rwork;
	Complex wkopt, *work;

	double *w = new double[n];

	matrix a;
	a.create(n);
	for(int r=0; r<n; ++r)
	{
		for(int c=0; c<n; ++c)
		{
		  a(r,c) = 0;

			if(c >= r)
				a(r,c) = (*this)(r,c);
		}
	}
	//a.print(std::cout);

	lwork = -1;
	lrwork = -1;
	liwork = -1;

	LAPACK_zheevd("N", "Upper", &n, a.getElements(), &lda, w, &wkopt, &lwork, &rwkopt, &lrwork, &iwkopt, &liwork, &info);
	lwork = (int)creal(wkopt);
	work = new Complex[lwork];
	lrwork = (int)rwkopt;
	rwork = new double[lrwork];
	liwork = iwkopt;
	iwork = new int[liwork];

	LAPACK_zheevd("N", "Upper", &n, a.getElements(), &lda, w, work, &lwork, rwork, &lwork, iwork, &liwork, &info);

	if(info == 0)
	{
		values.clear();
		for(int i=0; i<n; ++i)
			values.push_back(w[i]);
	}

	delete[] w;
	delete[] work;
	delete[] rwork;
	delete[] iwork;

	return (info==0);
}

void matrix::tensor(matrix* B, matrix* output)
{
	if(!B || !output)
		return;

	output->create(getRows()*B->getRows(), getCols()*B->getCols());

	for(int r=0; r<output->getRows(); ++r)
	{
		for(int c=0; c<output->getCols(); ++c)
		{
			int aRow = r / B->getRows();
			int aCol = c / B->getCols();
			int bRow = r % B->getRows();
			int bCol = c % B->getCols();

			output->setDirect(r, c, getDirect(aRow, aCol)* B->getDirect(bRow, bCol));
		}
	}
}

bool matrix::multiply(matrix& B, matrix& output, char TRANSB)
{
  if(TRANSB != 'N')
    std::cout << "Warning: matrix transpose not implemented in multiply function; result will be wrong\n";

  output.create(getRows(), B.getCols());
  Complex sum;
  for(int row=0; row<output.getRows(); ++row)
    {
      for(int col=0; col<output.getCols(); ++col){
	sum = 0;
	for(int k=0; k<getCols(); ++k)
	  sum = sum + ( (*this)(row,k) * B(k,col) );
	  
	output(row, col) = sum;
      }
    }
  return true;
}

}
