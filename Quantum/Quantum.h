/*
Quantum.h
Written by Walter O. Krawec

Copyright (c) 2016 Walter O. Krawec

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef QUANTUM_H_
#define QUANTUM_H_

#include <vector>
#include <list>

#include "Algebra.h"

namespace quantum
{
	// if empty, assume computational
	// otherwise B[i][j] is pr. amp of |j> for |i> where |j> is orth. and |i> is from applyOp (not nec. orth.)
	// that is, |i> is an "e" state and |i> = (B[i][0], B[i][1], ...)
	struct Basis
	{
	  std::vector < std::vector < Complex > >  B;
		void orthoganalize();

		Complex innerProduct(std::vector<Complex>& a, std::vector<Complex>& b)
		{
			Complex sum = 0 + 0*I;

			for(unsigned int i=0; i<a.size(); ++i)
			{
				Complex c = b[i], d;
				c = creal(b[i]) - cimag(b[i])*I;
				d = a[i] * c;

				sum = sum + d;
			}
			return sum;
		}

		double norm(std::vector <Complex>& a)
		{
			double sum = 0;
			Complex b,c;
			for(unsigned int i=0; i<a.size(); ++i)
			{
				c = a[i];
				c = creal(a[i]) - cimag(a[i])*I;
				b = a[i] * c;
				sum = sum + creal(b);
			}
			return sqrt(sum);
		}
	};

	struct KetBra
	{
		KetBra() {p = 0;};
		KetBra(int n)
		{
			ket.resize(n);
			bra.resize(n);
			for(int i=0; i<n; ++i)
			{
				ket[i] = 0;
				bra[i] = 0;
			}
			p = 0;
		};

		void project()	// takes |ket> and creates |ket><bra| (ignores "p")
		{
			bra = ket;
			return;
			for(unsigned int i=0; i<ket.size(); ++i)
				bra[i] = ket[i];
		}

		void print(std::ostream& f)
		{
		  f << "(" << creal(p) << "+" << cimag(p) << "i)|";
			for(unsigned int i=0; i<ket.size(); ++i)
				f << ket[i] << ",";
			f << "><";
			for(unsigned int i=0; i<bra.size(); ++i)
				f << bra[i] << ",";
			f << "|";
		}

	        Complex trace(std::vector <algebra::mat>& EVec, int ESpace);
		Complex trace(std::vector <Basis>& basis);
	        Complex trace();

		std::vector <int> ket, bra;
		Complex p;
	};

	class DensityList
	{
	public:
		// startSpace and endSpace index which spaces the operator should act on;
		// output is assumed startSpace, startSpace+1, ..., endSpace (one extra space used).
         	void applyAttackOp(int space, int actionSpace);
		void applyOp(int space);	// assume endSpace = space+1
		void applyOp(int startSpace, int endSpace);

		void applyOp(int space1, int space2, Basis& op);
		void applyOp(int space1, int space2, int space3, Basis& op);

		void applyOp(int space, algebra::mat& U);
		void applyOp(algebra::mat& U, int transitSpace);
		// applies "U" to space "space" if the state of "condSpace" is "cond" (i.e., applies a controlled "U")
		void applyConditionalOp(int condSpace, int cond, int space, algebra::mat& U);

		void swap(int space1, int space2);
		void measureAndSave(int measureSpace, int saveSpace);
		void measure(int measureSpace);

		double SafeLog(double x)
		{
		  if(x < 0.0000001)
		    return 0;
		  return log(x) / log(2.0);
		}
		double ShannonEntropy(); // computes entropy of diagonal elements
		double entropy(std::vector <algebra::mat>& EVec, int ESpace); // computes von Neumann entropy

		double calculatePr(int measureSpace, int state);
		double calculatePr(int space1, int state1, int space2, int state2, std::vector <algebra::mat>& EVec, int ESpace);
		double calculatePr(const std::vector <int>& spaceList, const std::vector <int>& stateList);

		// takes p and creates p1*p|0> + p2*p|1> (where |0>,|1> are in "space"); assumes "space" is |0>
		void splitAndSave(double p1, double p2, int space);

		void computeDensityMat(algebra::mat& output, std::vector <algebra::mat>& EVec, int ESpace);
		void computeDensityMat(std::vector <Basis>& basis, algebra::mat& output);
		void computeDensityMatFast(algebra::mat& output);

		void trace(int space, DensityList& output);
		void trace(int space);
		double trace(std::vector <algebra::mat>& EVec, int ESpace);
		void project(int space, int state, DensityList& output);
		void project(int space, int state);
		void CNOT(int controlSpace, int targetSpace);
		void NOT(int space);

		void applyHadamard(int space);
		void applyRotation(int space, double rotation);
		void applySU2(int space, double p, double theta, double psi);


		// Added Mar. 19 for new project
		void applyHadamard(int target, int control);
		void applyNOT(int target, int control);
		void applySU2(int target, int control, double p, double theta, double psi);

		// the following sets targetSpace = targetVal (for all elements in vector) if condSpace = condVal (for each ket and bra separately - measure afterwards)
		// be careful when using the procedure...
		void conditionalSet(std::vector <int>& targetSpace, std::vector <int>& targetVal, std::vector <int>& condSpace, std::vector <int>& condVal);

		void print(std::ostream& f)
		{
			std::list <KetBra>::iterator Iter;
			bool first = true;
			for(Iter = density.begin(); Iter != density.end(); ++Iter)
			{
			  if(creal(Iter->p) == 0 && cimag(Iter->p) == 0) continue;
			  if(!first)
			    f << " + ";
			  first = false;
			  Iter->print(f);
			}

			std::cout << "\n\n";
		}
	//private:
		std::vector <int> dimension;
		std::list <KetBra> density;
	};
}

#endif
