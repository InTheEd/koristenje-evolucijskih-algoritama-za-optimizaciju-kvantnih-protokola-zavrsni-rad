/*
Algebra.h
Written by Walter O. Krawec

Copyright (c) 2016 Walter O. Krawec

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef _ALGEBRA_H_
#define _ALGEBRA_H_

#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <complex.h>

#define Complex double _Complex

namespace algebra
{

	class ComplexHelper
	{
	public:
	  static Complex one() {Complex out; out = 1.0 + 0*I; return out; }
	  static Complex zero() {Complex out; out = 0 + 0*I; return out;}

		static void sq(Complex& v)
		{
		  Complex v2 = creal(v) - cimag(v)*I;
		  v = mult(v, v2);
		}
		static Complex construct(double theta)
		{
		  Complex out = cos(theta) + sin(theta)*I;
		  return out;
		}

		static Complex power(Complex& base, int e)
		{
		  return cpow(base, e);
			Complex a;
			double real = log( sqrt( creal(base)*creal(base)* + cimag(base)*cimag(base)));
			double imag = atan2(cimag(base), creal(base));

			real *= e;
			imag *= e;

			a = real + imag*I;

			Complex result = construct(cimag(a));


			Complex r2 = creal(result)*exp(real) + cimag(result)*real*I;

			return r2;
		}

		static Complex add(Complex& a, Complex& b)
		{
		  return a+b;
		}
		static Complex subtract(Complex& a, Complex& b)
		{
		  return a-b;
		}

		static Complex mult(Complex& a, Complex& b)
		{
		  return a*b;
		}

		static Complex invert(Complex& a)
		{
			Complex out;
			double d = creal(a)*creal(a) + cimag(a)*cimag(a);

			out = creal(a) / d - cimag(a)/d *I;

			return out;
		}
	};

	class matrix
	{
	public:
		matrix()
		{
			elements = NULL;
			noCleanup = false;
			r = 0;
			c = 0;
			n = 0;
		}
		matrix(const char* _name)
		{
			elements = NULL;
			noCleanup = false;
			r = 0;
			c = 0;
			n = 0;
			varName = std::string(_name);
		}
		matrix(const matrix& m)
		{
			elements = NULL;
			r = m.r;
			c = m.c;
			n = r*c;

			if(r == 0 || c == 0 || !m.elements)
				return;

			elements = new Complex[n];

			noCleanup = false;

			for(int i=0; i<n; ++i)
				elements[i] = m.elements[i];
		}
		~matrix() {cleanup();}

		void saveToFile(const std::string& filename);
		void saveToFile(std::ofstream& f);

		bool loadFromFile(const std::string& filename);
		bool loadFromFile(std::ifstream& f);

		matrix& operator=(const matrix& m)
		{
			if(!elements || r != m.r || c != m.c)
			{
				cleanup();
				r = m.r;
				c = m.c;
				n = r*c;

				if(r == 0 || c == 0 || !m.elements)
				{
					throw std::string("operator= error.\n");
					return *this;
				}

				elements = new Complex[n];
			}

			if(!m.elements)
			{
				throw std::string("operator= error 2.\n");
				return *this;
			}

			for(int i=0; i<n; ++i)
				elements[i] = m.elements[i];

			noCleanup = false;

			return *this;
		}

		void cleanup()
		{
			if(elements && !noCleanup)
			{
				delete[] elements;
			}
			elements = NULL;
			r = 0;
			c = 0;
			n = 0;
		}

		void zero()
		{
			if(!elements)
				return;

			for(int i=0; i<n; ++i)
			{
			  elements[i] = 0.0+0.0*I;
			}
		}

		void ident()
		{
			if(!elements)
				return;

			zero();
			int dim = getRows();
			if(getCols() < getRows())
				dim = getCols();

			Complex one = 1.0+0.0*I;

			for(int i=0; i<dim; ++i)
				this->setDirect(i,i,one);
		}

		Complex trace()
		{
			Complex c = 0.0 + 0.0*I;
			for(int i=0; i<getRows(); ++i)
			{
			  c = c + (*this)(i,i);
			}
			return c;
		}

		void clear(Complex value)
		{
			if(!elements)
				return;
			for(int i=0; i<n; ++i)
				elements[i] = value;
		}

		bool create(int _r, int _c, Complex* _elements)
		{
			cleanup();
			r = _r;
			c = _c;
			n = r*c;

			elements = _elements;
			noCleanup=true;
			return true;
		}
		bool create(int _r, int _c)
		{
			if(_r <= 0 || _c <= 0)
				return false;
			if(elements && r == _r && c == _c)
				return true;

			cleanup();
			r = _r;
			c = _c;
			n = r*c;

			elements = new Complex[r*c];

			noCleanup = false;

			return true;
		}

		bool create(int _r)
		{
			return create(_r, _r);
		}

		Complex& operator() (int row, int col)
		{
			int _r = row;
			int _c = col;
			if(!elements || _r >= r || _c >= c || _r < 0 || _c < 0)
			{
				std::cout << "Error 5 (" << elements << ", " << _r << ", " << r << ", " << _c << ", " << c << ")\n";
				std::cout << "Name = " << varName << "\n";
				throw(std::string("Algebra Error 5.1"));
				//throw(5);
				//return double();
			}

			return elements[_c*r + _r];
		}

		void random_unitary(const std::vector <double>& phi_v, const std::vector <double>& psi_v, const std::vector <double>& chi_v, double alpha);
		bool eigenValues(std::vector <double>& values);
		double safeLog(double x)
		{
		  if(x < 0.0000001)
		    return 0;
		  return log(x)/log(2.0);
		}
		double entropy()
		{
		  std::vector<double> ev;
		  double sum = 0;
		  
		  eigenValues(ev);
		  
		  for(int i=0; i < ev.size(); ++i)
		    {
		      sum = sum - ev[i] * safeLog(ev[i]);
		    }

		  return sum;
		}


		Complex getDirect(int row, int col)
		{
			int _r = row;
			int _c = col;
			if(!elements || _r >= r || _c >= c || _r < 0 || _c < 0)
			{
				std::cout << "Error 5 (" << elements << ", " << _r << ", " << r << ", " << _c << ", " << c << ")\n";
				throw(std::string("Algebra Error 5.2"));
				//throw(5);
				//return double();
			}

			return elements[_c*r + _r];
		}

		void setDirect(int _n, Complex data)
		{
			if(!elements || _n >= n)
			{
//				std::cout << "Error 5 (" << elements << ", " << _r << ", " << r << ", " << _c << ", " << c << ")\n";
				throw(std::string("Algebra Error 5.3"));
				//throw(5);
				//return double();
			}

			elements[_n] = data;
		}

		void setDirect(int row, int col, Complex data)
		{
			int _r = row;
			int _c = col;
			if(!elements || _r >= r || _c >= c || _r < 0 || _c < 0)
			{
				std::cout << "Error 5 (" << elements << ", " << _r << ", " << r << ", " << _c << ", " << c << ")\n";
				throw(std::string("Algebra Error 5.4"));
				//throw(5);
				//return double();
			}

			elements[_c*r + _r] = data;
		}

		void setDirect(int row, int col, double re, double im)
		{
			int _r = row;
			int _c = col;
			if(!elements || _r >= r || _c >= c || _r < 0 || _c < 0)
			{
				std::cout << "Error 5 (" << elements << ", " << _r << ", " << r << ", " << _c << ", " << c << ")\n";
				throw(std::string("Algebra Error 5.4"));
				//throw(5);
				//return double();
			}

			elements[_c*r + _r] = re + im*I;
		}
	        Complex get(int _n)
		{
			if(!elements || _n >= n)
			{
				std::cout << "Error 2 (" << elements << ", " << _n << ", " << n << ")\n";
				return 0;
			}

			return elements[_n];
		}

		Complex* getElements()
		{
			return elements;
		}

		void print(std::ostream& f, bool printAll=false)
		{
			if(!elements)
				return;
			int rStart = 0, cStart=0;
		//	if(r > 10)
			//	rStart = r-10;
		//	if(c > 10)
		//		cStart = c-10;

			int rows = alg_min(10, r);
			int cols = alg_min(10, c);

			if(printAll)
			{
				rows = r;
				cols = c;
			}
			for(int _r=rStart; _r < rows; ++_r)
			{
				for(int _c=cStart; _c<cols; ++_c)
				{
				  f << creal((*this)(_r,_c)) << "+" << cimag((*this)(_r,_c)) << "i\t";
				}
				f << "\n";
			}

			f << "\n";
		}

		void multiplyScalar(Complex& c)
		{
			if(!elements)
				return;

			for(int i=0; i<n; ++i)
			{
				elements[i] = ComplexHelper::mult(elements[i], c);
			}
		}

		void sqTranspose()
		{
			if(!elements || c != r)
			{
				throw(std::string("Algebra error 1.2"));
				return;
			}

			Complex temp;
			for(long row=0; row<r; ++row)
			{
				for(long col=row; col<c; ++col)
				{
					if(row == col)
						continue;
					temp = getDirect(row,col);
					setDirect(row,col, getDirect(col,row));
					setDirect(col,row, temp);
				}
			}
		}

		void transpose(matrix& B)
		{
			if(!elements)
				return;
			B.create(c,r);
			for(int _r=0; _r<r; ++_r)
			{
				for(int _c=0; _c<c; ++_c)
				{
				  Complex c = (*this)(_r, _c);
				  B(_c, _r) = creal(c) - cimag(c)*I;
				}
					//B.set(_c,_r, get(_r,_c));
			}
		}

		void subtract(Complex& c)
		{
			if(!elements)
				return;

			for(int i=0; i<n; ++i)
			{
			  elements[i] = elements[i] = c;
			}
		}

		void subtract(matrix* B, matrix* output)
		{
			if(!B || !output)
				return;

			if(!elements || !B->getElements() || getRows() != B->getRows() || getCols() != B->getCols())
			{
				throw std::string("Error - cannot subtract matrix\n");
				return;
			}

			output->create(getRows(), getCols());

			for(int i=0; i<n; ++i)
			{
			  output->elements[i] = elements[i] - B->getElements()[i];
			}
		}

		void add(matrix* B, matrix* output)
		{
			if(!B || !output)
				return;

			if(!elements || !B->getElements() || getRows() != B->getRows() || getCols() != B->getCols())
			{
				throw std::string("Error - cannot add matrix\n");
				return;
			}

			output->create(getRows(), getCols());

			for(int i=0; i<n; ++i)
			{
			  output->elements[i] = elements[i] + B->getElements()[i];
			}
		}

		void random(double smallValue, double largeValue);
		void add(matrix* B)
		{
			matrix* output = this;
			if(!B)
				return;

			if(!elements || !B->getElements() || getRows() != B->getRows() || getCols() != B->getCols())
			{
				throw std::string("Error - cannot add matrix\n");
				return;
			}

			//output->create(getRows(), getCols());

			for(int i=0; i<n; ++i)
			{
			  output->elements[i] = elements[i] + B->getElements()[i];
			}
		}

		// this = this + c*B
		void add(Complex c, matrix& B)
		{
			matrix* output = this;

			if(!elements || !B.getElements() || getRows() != B.getRows() || getCols() != B.getCols())
			{
				throw std::string("Error - cannot add matrix\n");
				return;
			}

			//output->create(getRows(), getCols());

			for(int i=0; i<n; ++i)
			{
			  output->elements[i] = elements[i] + c*B.getElements()[i];
			}
		}


		void dot(matrix* B, Complex& output);	// returns this <dot> B
		// set Trans* = 'T' for *^T
		bool multiply(matrix& B, matrix& output, char TRANSB='N');

		void tensor(matrix* B, matrix* output);

		int getRows()
		{
			return r;
		}
		int getCols()
		{
			return c;
		}
		int getSize()
		{
			return n;
		}
	private:
		int alg_min(int a, int b)
		{
			return (a < b) ? a : b;
		}
		int alg_max(int a, int b)
		{
			return (a > b) ? a : b;
		}
		Complex* elements;
		int r, c, n;

		bool noCleanup;

		std::string varName;
	};

	typedef matrix mat;
};

#endif
