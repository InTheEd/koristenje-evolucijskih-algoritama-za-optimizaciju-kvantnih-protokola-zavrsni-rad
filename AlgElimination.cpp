#include "ecf/ECF_base.h"
#include "AlgElimination.h"
#include "ecf/ECF_macro.h"


Elimination :: Elimination()
{
	name_ = "Elimination";

	selFitPropOp = static_cast<SelFitnessProportionalOpP> (new SelFitnessProportionalOp);
	selRandomOp = static_cast<SelRandomOpP> (new SelRandomOp);
	selBestOp = static_cast<SelBestOpP> (new SelBestOp);
}


void Elimination :: registerParameters(StateP state)
{
	registerParameter(state, "gengap", (voidP) new double(0.6), ECF::DOUBLE,
		"generation gap (percentage of population to be eliminated)");
	registerParameter(state, "selpressure", (voidP) new double(10), ECF::DOUBLE,
		"selection pressure: how much is the worst individual 'worse' than the best");
}


bool Elimination :: initialize(StateP state)
{
	selFitPropOp->initialize(state);
	selRandomOp->initialize(state);
	selBestOp->initialize(state);

	voidP genGapP = getParameterValue(state, "gengap");
	genGap_ = *((double*) genGapP.get());

	if(genGap_ <= 0 || genGap_ > 1) {
		ECF_LOG_ERROR(state, "Error: generation gap parameter in Elimination algorithm must be in <0, 1]!");
		throw "";
	}

	voidP selPressP = getParameterValue(state, "selpressure");
	selPressure_ = *((double*) selPressP.get());

	// selection chooses worse individuals
	selFitPropOp->setSelPressure(1./selPressure_);

	return true;
}


bool Elimination :: advanceGeneration(StateP state, DemeP deme)
{
	// elitism: copy current best individual
	IndividualP best = selBestOp->select(*deme);
	best = copy(best);

	// copy pointers
	std::vector<IndividualP> newGen;
	for(uint i = 0; i < deme->size(); i++)
		newGen.push_back(deme->at(i));

	// eliminate genGap_ worse individuals
	uint generationGap = (uint) (genGap_ * deme->size());
	for(uint i = 0; i < generationGap; i++) {
		IndividualP victim = selFitPropOp->select(newGen);
		removeFrom(victim, newGen);
	}

	// make genGap_ new ones
	for(uint i = 0; i < generationGap; i++) {

		DemeP dem = state->getPopulation()->at(0);
		std::vector<std::vector<IndividualP>> parents;
		std::vector<uint> covered;
		for (uint i = 0; i < dem->size(); i++) {
			IndividualP temp1 = dem->at(i);
			uint size1 = temp1->getGenotype()->getGenomeSize();
			if (std::find(covered.begin(), covered.end(), size1) != covered.end()) {
				continue;
			}
			covered.push_back(size1);
			std::vector<IndividualP> temp_par;
			temp_par.push_back(temp1);
			for (uint j = i + 1; j < dem->size(); j++) {
				IndividualP temp2 = dem->at(j);
				uint size2 = temp2->getGenotype()->getGenomeSize();
				if (size1 == size2) {
					temp_par.push_back(temp2);
				}
			}
			if (temp_par.size() > 1) {
				parents.push_back(temp_par);
			}
		}
		int randomIndex = rand() % parents.size();
		int randomIndex2 = rand() % parents[randomIndex].size();
		IndividualP parent1 = parents[randomIndex][randomIndex2];
		int randomIndex3 = rand() % parents[randomIndex].size();
		while (randomIndex3 == randomIndex2) {
			randomIndex3 = rand() % parents[randomIndex].size();
		}
		IndividualP parent2 = parents[randomIndex][randomIndex3];

		IndividualP child = copy(parent1);
		mate(parent1, parent2, child);
		newGen.push_back(child);
	}

	// replace
	for(uint i = 0; i < deme->size(); i++)
		replaceWith(deme->at(i), newGen[i]);

	// mutate all
	mutate(*deme);

	// evaluate new individuals
	for(uint i = 0; i < deme->size(); i++)
		if(!deme->at(i)->fitness->isValid()) {
			evaluate(deme->at(i));
		}

	// elitism: preserve best individual
	IndividualP random = selFitPropOp->select(*deme);
	if(best->fitness->isBetterThan(random->fitness))
		replaceWith(random, best);

	return true;
}
