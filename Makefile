CXX = g++
WARN = -Wall
LAPACKDIR=/home/student/Downloads/lapack/lapack-3.9.0
ATLASDIR=/usr/lib/x86_64-linux-gnu/blas/

CFLAGS = $(WARN)
LDFLAGS = $(WARN) -lecf -L/usr/lib/x86_64-linux-gnu -L$(ATLASDIR) -lblas -Wl,--start-group $(LAPACKDIR)/librefblas.a $(LAPACKDIR)/liblapacke.a $(LAPACKDIR)/liblapack.a -Wl,--end-group -lgfortran


OBJS = AlgElimination.o AlgRouletteWheel.o Individual.o QuantumEvalOp.o main.o Algebra.o Quantum.o  attack.o \
	IntGenotype.o IntGenotypeCrxAverage.o IntGenotypeCrxOp.o IntGenotypeCrxTwoPoint.o IntGenotypeMutOp.o

BINFILE = quantum

.PHONY = all clean

all: $(BINFILE)

$(BINFILE): $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o $(BINFILE)

QuantumEvalOp.o: QuantumEvalOp.cpp QuantumEvalOp.h
	$(CXX) $(CFLAGS) -c QuantumEvalOp.cpp

Individual.o: Individual.cpp Individual.h
	$(CXX) $(CFLAGS) -c Individual.cpp

AlgRouletteWheel.o: AlgRouletteWheel.cpp AlgRouletteWheel.h
	$(CXX) $(CFLAGS) -c AlgRouletteWheel.cpp

AlgElimination.o: AlgElimination.cpp AlgElimination.h
	$(CXX) $(CFLAGS) -c AlgElimination.cpp

IntGenotype.o: IntGenotype.cpp IntGenotype.h
	$(CXX) $(CFLAGS) -c IntGenotype.cpp -c IntGenotypeCrxAverage.cpp -c IntGenotypeCrxOp.cpp \
	-c IntGenotypeCrxTwoPoint.cpp -c IntGenotypeMutOp.cpp

main.o: main.cpp QuantumEvalOp.h
	$(CXX) $(CFLAGS) -c main.cpp

Algebra.o: Quantum/Algebra.cpp Quantum/Algebra.h
	g++ -O3 -c Quantum/Algebra.cpp -I /usr/include -I $(LAPACKDIR)/LAPACKE/include

Quantum.o: Quantum/Quantum.cpp Quantum/Quantum.h
	g++ -O3 -c Quantum/Quantum.cpp

attack.o: attack.cpp attack.h
	g++ -O3 -fpermissive -c attack.cpp 

clean:
	rm -f *~ $(OBJS) $(BINFILE)
