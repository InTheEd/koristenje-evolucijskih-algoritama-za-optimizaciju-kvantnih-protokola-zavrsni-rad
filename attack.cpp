/*
attack.cpp
Written by Walter O. Krawec, Michael Nelson, and Eric Geiss
Copyright (c) 2017

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include "attack.h"

namespace QKD
{

  double Stats::BB84Rate()
  {
    double R01 = p0a - .5;
    double R23 = p1a - .5;
    double R02 = pa0 - .5*(1-p01+p10);
    double R13 = -R02;

    double I01 = p0b - .5;
    double I23 = p1b - .5;
    double I02 = .5*(1-p01+p10) - pb0;
    double I13 = -I02;

    double R03 = 1 - Qx - Qy - .5*(R01+R23+I01+I23);
    double R12 = Qy - Qx + .5*(I01+I23-R01-R23);

    double l01 = .5 + sqrt( (p10-p01)*(p10-p01) + 4*R03*R03 ) / (2.0*(2-p01-p10));
    double l23 = .5 + sqrt( (p01-p10)*(p01-p10) + 4*R12*R12 ) / (2.0*(p01+p10));

    double first = entropy( (1-p01) / (2.0-p01-p10) ) - entropy(l01);
    double second = entropy( p01 / (p01+p10) ) - entropy(l23);

    if(p01+p10 <= .0000001)
      second = 0;
    if(2.0-p01-p10 <= 0.0000001)
      first = 0;

    double SAE = (2-p01-p10)/2.0 * first + (p01+p10)/2.0 * second;

    double key00 = .5*(1-p01);
    double key11 = .5*(1-p10);
    double key01 = .5*p01;
    double key10 = .5*p10;

    double H = HAB(key00,key01,key10,key11);

    return SAE - H;
  }

  void Stats::symmetric(double Q)
  {
    p00 = 1-Q;
    p01 = Q;
    p10 = Q;
    p11 = 1-Q;
    
    Qx = Q;
    Qy = Q;
    
    p0a = .5;
    p1a = .5;
    pa0 = .5;
    
    p0b = .5;
    p1b = .5;
    pb0 = .5;
  }

}
