#include<cmath>
#include<iostream>
#include <list>

// for Windows development only
#define SIMULATION


#ifndef SIMULATION
#include "Quantum/Quantum.h"
#endif


namespace QKD
{
	const int nGateTypes = 4;

  enum GateType
  {
    NOT=0,
    Rotation,
    Hadamard,

    Measure
  };

  struct Gate
  {
    Gate()
    {
      type = 0;
      controlMode = false;
      target = 0;
      control = 0;
      p = 0;
      theta = 0;
      psi = 0;
    }
    int type;
    bool controlMode;
    int target, control;
    double p, theta, psi;
  };
}

double SafeLog(double x)
{
  if(x < 0.000000001)
    return 0;
  return log(x) / log(2.0);
}
double entropy(double x)
{
  return -x*SafeLog(x) - (1-x)*SafeLog(1-x);
}
double entropy(double a, double b, double c, double d)
{
  return -a*SafeLog(a) - b*SafeLog(b) - c*SafeLog(c) - d*SafeLog(d);
}


#ifdef SIMULATION

double ComputeFitness(std::list <QKD::Gate>& A, std::list <QKD::Gate>& B)
{
	return 1;
}

#else

double ComputeFitness(std::list <QKD::Gate>& A, std::list <QKD::Gate>& B)
{
  const int T = 0;
  const int keyA = 1;
  const int arA = 2;
  const int keyB = 3;
  const int arB = 4;
  const int NUM_WIRES = 5;

  const int ASpace = 2;  // largest wire A is allowed access to

  quantum::DensityList dl;

  for(int i=0; i<NUM_WIRES; ++i)
    dl.dimension.push_back(2);

  quantum::KetBra kb(NUM_WIRES);
  kb.p = 1;
  dl.density.push_back(kb);

  // run A:
  if(!A.empty()){
    std::list<QKD::Gate>::iterator Iter;
    for(Iter = A.begin(); Iter != A.end(); ++Iter){
      int target = Iter->target;
      int control = Iter->control;
      if(!Iter->controlMode)
	control = -1;

      // ensure that gates aren't applied to wires that cannot be accessed:
      if(target > ASpace || control > ASpace)
	continue;

      switch(Iter->type){
      case QKD::Hadamard: dl.applyHadamard(target, control);
	break;

      case QKD::NOT: dl.applyNOT(target, control);
	break;

      case QKD::Rotation: dl.applySU2(target, control, Iter->p, Iter->theta, Iter->psi);
	break;

      case QKD::Measure: dl.measure(target);
	break;
      }
    }
  }

  // E attacks (todo)

  // Run B:
  if(!B.empty()){
    std::list<QKD::Gate>::iterator Iter;
    for(Iter = B.begin(); Iter != B.end(); ++Iter){
      int target = Iter->target;
      int control = Iter->control;
      if(!Iter->controlMode)
	control = -1;

      // ensure that gates aren't applied to wires that cannot be accessed:
      if( (target > T && target <= ASpace) || (control > T && control <= ASpace) )
	continue;

      switch(Iter->type){
      case QKD::Hadamard: dl.applyHadamard(target, control);
	break;

      case QKD::NOT: dl.applyNOT(target, control);
	break;

      case QKD::Rotation: dl.applySU2(target, control, Iter->p, Iter->theta, Iter->psi);
	break;

      case QKD::Measure: dl.measure(target);
	break;
      }
    }
  }

  // now A measures key
  dl.measure(keyA);

  // B measures key
  dl.measure(keyB);

  // project to both users accepting (conditioning):
  dl.project(arA, 1);
  dl.project(arB, 1);

  // check that there is something here (always possible to "reject" with probability 1)
  if(dl.density.empty())
    return 0;  // both parties are always saying 'reject' so no key so key-rate = 0.

  // compute S(A|E)
  // todo
  double SAE = 1;

  // compute H(A|B):

  double p00 = dl.calculatePr(keyA, 0, keyB, 0);
  double p01 = dl.calculatePr(keyA, 0, keyB, 1);
  double p10 = dl.calculatePr(keyA, 1, keyB, 0);
  double p11 = dl.calculatePr(keyA, 1, keyB, 1);

  double HAB = entropy(p00, p01, p10, p11);
  double HB = entropy(p00 + p10);

  // ToDo: Add option for one or the other....
  // conditional key-rate:
  ////return SAE - (HAB - HB);

  double paccept = dl.trace();
  // actual key-rate:
  return paccept*(SAE - (HAB-HB));
}
#endif


void testBB84()
{
  std::list <QKD::Gate> A, B;
  QKD::Gate g;
  g.type = QKD::NOT;
  g.target = 2;
  g.controlMode = false;

  A.push_back(g); // g1

  g.type = QKD::Hadamard;
  g.target = 1;
  g.controlMode = false;

  A.push_back(g); // g2

  g.type = QKD::NOT;
  g.target = 0;
  g.controlMode = true;
  g.control = 1;

  A.push_back(g); // g3


  g.type = QKD::NOT;
  g.target = 3;
  g.control = 0;
  g.controlMode = true;

  B.push_back(g); // h1

  g.type = QKD::NOT;
  g.target = 4;
  g.controlMode = false;

  B.push_back(g); // h2


  std::cout << "Test 1: key-rate (should be 1) = " << ComputeFitness(A,B) << "\n\n";

  g.type = QKD::Hadamard;
  g.target = 4;
  g.controlMode = false;
  B.push_back(g);

  std::cout << "Test 2: key-rate (should be 0.5) = " << ComputeFitness(A,B) << "\n\n";

  g.target = 2;
  A.push_back(g);

  std::cout << "Test 3: key-rate (should be 0.25) = " << ComputeFitness(A,B) << "\n\n";
}
